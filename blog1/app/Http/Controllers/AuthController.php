<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Auth(){
        return view('register');
    }
    public function wellcome_post(Request $request){
        $depan = $request['frist_name'];
        $belakang = $request['last_name'];
        return view('wellcome', ['frist_name' => $depan, 'last_name' => $belakang]);
    }
}
