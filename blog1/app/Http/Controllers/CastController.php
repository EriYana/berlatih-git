<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
public function index(){
    $cast = DB::table('cast')->get();
    return view('cast.cast', compact('cast'));
}
public function create(){
    return view('cast.create');
}
public function store(Request $request){
    $request->validate([
        'nama' => 'required|max:255',
        'umur' => 'required',
        'bio' => 'required',
    ],
    [ 
        'nama.required' => 'NAMA CAST TIDAK BOLEH KOSONG!',
        'umur.required' => 'HARAP MASUKAN UMUR!',
        'bio.required' => 'BIO CAST TIDAK BOLEH KOSONG HARAP DIISI!'

    ]
   );

   DB::table('cast')->insert(
    [
        'nama' => $request['nama'],
        'umur' => $request['umur'],
        'bio' => $request['bio']
    
    ]
);
 return redirect('/cast');
}
 public function show($id){
    $cast = DB::table('cast')->where('id', $id)->first();

    return view('cast.detail', compact('cast'));
 }
 public function edit($id){
    $cast = DB::table('cast')->where('id', $id)->first();

    return view('cast.update', compact('cast'));
 }
public function update(Request $request, $id){
    $request->validate([
        'nama' => 'required|max:255',
        'umur' => 'required',
        'bio' => 'required',
    ],
    [ 
        'nama.required' => 'NAMA CAST TIDAK BOLEH KOSONG!',
        'umur.required' => 'HARAP MASUKAN UMUR!',
        'bio.required' => 'BIO CAST TIDAK BOLEH KOSONG HARAP DIISI!'

    ]
   );
   DB::table('cast')
   ->where('id', $id)
   ->update(
    [
        'nama' => $request['nama'],
        'umur' => $request['umur'],
        'bio' => $request['bio'] 
    ]
   );

   return redirect('/cast');

}
public function destroy($id){
 DB::table('cast')->where('id', $id)->delete();
 return redirect('/cast');
}

}
