<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@Auth');
Route::post('wellcome', 'AuthController@Wellcome_post');
Route::get('/master', function(){
    return view('layout.master');
});
Route::get('/table', function(){
    return view('table');
});
Route::get('/data-table', function(){
    return view('data-table');
});
Route::get('/cast', 'CastController@index');
//MEMBUAT DATA 
Route::get('/cast/create', 'CastController@create');
//MENAMBAHKAN KE DATABASE
Route::post('/cast', 'CastController@store');
//MENGUODATE DATA   

Route::get('/cast/{id}', 'CastController@show'); 
//Edit data ke database
Route::get('/cast/{id}/edit', 'CastController@edit');
//update data ke database
Route::put('/cast/{id}', 'CastController@update');
//Delete data ui dan database
Route::delete('/cast/{id}', 'CastController@destroy');