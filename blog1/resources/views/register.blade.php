<!DOCTYPE html>
<html lang="en">
    @extends('Layout.master')
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    @section('judul')
    Register
    @endsection
    @section('content')
        
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form </h2>
    <form action="/wellcome" method="POST">
            @csrf
            <label>Frist Name:</label> <br><br>
            <input type="text" name="frist_name"> <br> <br>
            <label> Last Name:</label> <br><br>
            <input type="text" name="last_name"> <br> <br>
            <label> Gender:</label> <br> <br>
            <input type="radio" name="G">Male <br>
            <input type="radio" name="G">Famale <br>
            <input type="radio"  name="G">Other
            <br> <br>
            <label>Nationality:</label>
            <br> <br>
            <select name="Nationality">
                <option value="Indonesia">Indonesia</option>
                <option value="Malaysia">Malaysia</option>
                <option value="Brazil">Brazil</option>
                <option value="China">China</option>
            </select>
            <br><br>
            <label>Language Spoken:</label>
            <br><br>
            <input type="checkbox">Bahasa Indoensia
            <br>
            <input type="checkbox">English
            <br>
            <input type="checkbox"> Other
            <br><br>
            <label>Bio:</label>
            <br><br>
            <textarea name="message" cols="30" rows="10"></textarea><br>
            <input type="submit" value="SignUp">
        </form>
        @endsection
    </body>
    </html>