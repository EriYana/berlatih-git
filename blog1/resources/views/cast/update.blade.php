@extends('layout.master')
@section('judul')
    Halamaan update Cast
@endsection
@section('content')
<form action="/cast/{{ $cast ->id }}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <input type="text" class="form-control" name="nama" value="{{ old('umur', $cast->nama) }}"  placeholder="Tuliskan Nama">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
        <input type="number" value="{{ old('umur', $cast->umur) }}" class="form-control" name="umur"placeholder="Masukan Umur"> 
      </div>
      @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
      <div class="form-group">
    </div>
    <textarea  name="bio" cols="40" rows="8">{{ old('umur', $cast->bio) }}</textarea>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection