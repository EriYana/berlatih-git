@extends('layout.master')

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <input type="text" class="form-control" name="nama"" placeholder="Tuliskan Nama">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
        <input type="number" class="form-control" name="umur"  placeholder="Masukan Umur">
      </div>
      @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
      <div class="form-group">
        <textarea name="bio" cols="40" rows="8"></textarea>
      </div>
      @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection