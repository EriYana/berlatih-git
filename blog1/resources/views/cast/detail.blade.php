@extends('layout.master')
@section('judul')
Halaman Detail cast 
@endsection
@section('content')
<a href="/cast" class="btn btn-primary btn-sm md-2 mb-4">KEMBALI</a>
    <h1 class="text-info text-5xl">Nama : {{$cast->nama}}</h1>
        <h3>Umur : {{$cast->umur}}</h3>
        <p class="text-lg">Bio data {{ $cast->nama }} : {{ $cast->bio }}</p>
@endsection