@extends('layout.master')
@section('judul')
    Halaman Tampilan semua data cast
@endsection
    @section('content')
        <a href="/cast/create" class="btn btn-primary  btn-sm md-4">Tambah Cast</a>
    <div class="row">
        <div class="col-12">
          <div class="card">
            <!-- ./card-header -->
            <div class="card-body">
              <table class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr data-widget="expandable-table" aria-expanded="false">
                   @forelse ($cast as $key => $item)
                   <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->umur}}</td>
                    <td>
                        <form action="/cast/{{ $item->id }}" method="POST" >
                            @csrf
                            @method('delete')
                        <a href="/cast/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                       <a href="/cast/{{ $item->id }}/edit" class="btn btn-warning btn-sm ml-4">Edit Data</a>
                        <input type="submit" class="btn btn-danger btn-sm ml-4" value="delete">
                       </form>
                    </td>
                   </tr>
                   @empty
                   <tr>
                    <td>Tidak ada data cast</td>
                   </tr>
                   @endforelse
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
        @endsection
    